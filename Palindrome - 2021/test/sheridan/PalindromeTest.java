package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		assertTrue("Not a Palindrome", Palindrome.isPalindrome("dad"));
	}

	@Test
	public void testIsPalindromeNegative( ) {
		assertFalse("Not a Palindrome", Palindrome.isPalindrome("not a palindrome"));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue("Not a Palindrome", Palindrome.isPalindrome("edit tide"));
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse("Not a Palindrome", Palindrome.isPalindrome("edit on tide"));
	}	
	
}
